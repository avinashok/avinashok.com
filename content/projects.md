+++
title = "Projects"
slug = "projects"
thumbnail = "images/tn.png"
description = "projects"
+++

## ML Projects

I. Text Categorization using Machine Learning
Different approaches to do Text Categorization and a framework to compare the results of those approaches.
[Github Link](https://github.com/avinashok/TextCategorization)
Tools: Python, Jupyter Notebook.

II.	Topic Modeling
Codes explaining the concept.
[Github Link](https://github.com/avinashok/TopicModeling)
Tool: R Programming, Python, PCF

III. Web Scraping
Demonstrating web scraping using Selenium & subsequent text mining.
[Github Link](https://github.com/avinashok/TopicModeling)


## Other Projects
TKM College magazine editor during the academic year 2015-16. The magazine went on to receive prestigious Basheer award for one of the best college magazines in the state and Malayala Manorama Chief Editor’s trophy in best poetry segment.