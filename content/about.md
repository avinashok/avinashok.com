+++
title = "About Avinash"
slug = "about"
thumbnail = "images/tn.png"
description = "about"
+++

---------------------------
Machine Learning Engineer who likes to solve challenging business problems using various advanced analytics and data-driven insights. Have worked with various Fortune 100 clients, helping them make optimized-profitable decisions. I am primarily focused on Natural Language Processing and cloud-based application development and deployment, but also have hands-on experience working in statistics, time series analysis, predictive data modeling, various model families, neural networks and computer vision.

Currently working as ML Engineer at [EY Spain](https://www.ey.com/es_es). Worked as a Data Scientist in the Applied AI team at [Deloitte Consulting USI](https://www2.deloitte.com/in/en/services/consulting-deloitte.html?icid=bottom_consulting-deloitte#). I completed my Bachelor of Technology in Mechanical Engineering from [TKM College of Engineering](https://www.tkmce.ac.in/). Previously worked in Advanced Analytics & AI team of [EY GDS](https://www.ey.com/en_in/careers/global-delivery-services). I was also college union magazine editor for ["Kallu magazine"](https://www.facebook.com/collegemagazinetkmce.2016) which won third place in [Basheer Awards](https://www.deshabhimani.com/news/kerala/news-kerala-16-07-2016/575257) for best college magazines in Malayalam.

I believe in rational thinking and endorse open sourcing research and projects. I love physics and programming. Whenever in doubt, I like to go back to first principles and simplicity.

Outside of work, I enjoy:
- watching & playing football. Ardent fan of Liverpool FC.
- listening to [music](https://open.spotify.com/playlist/1OC5w2BrNnDEMptBZ3FWvw?si=0XxdyMg4Tl2jZuq4rpXz0w&utm_source=copy-link).
- watching movies.

If you have any message/feedback or an innovative idea to share/collaborate on, let me know [here](https://docs.google.com/forms/d/e/1FAIpQLSc7GsQz8hkT3Em6ABkh2EW9lULyL6qEf0_ufrmQuQPvWBU4Ww/viewform?usp=pp_url).
